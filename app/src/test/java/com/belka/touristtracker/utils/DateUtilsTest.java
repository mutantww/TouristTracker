package com.belka.touristtracker.utils;

import org.junit.Test;
import java.util.Date;
import java.text.SimpleDateFormat;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Volodymyr on 28.03.2018.
 */
public class DateUtilsTest {
    @Test
    public void getDateDifference() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy HH:mm");
        Date dateOne = null;
        Date dateTwo = null;
        String date1 = "";
        String date2 = "";
        String message = "";

        /**тест на минуты*/
        date1 = "02.04.18 00:25";
        date2 = "02.04.18 00:35";
        dateOne = format.parse(date1);
        dateTwo = format.parse(date2);
        message = DateUtils.getDateDifference(dateOne, dateTwo);
        System.out.println(message);

        /**тест на часы и минуты*/
        date1 = "02.04.18 00:25";
        date2 = "02.04.18 02:26";
        dateOne = format.parse(date1);
        dateTwo = format.parse(date2);
        message = DateUtils.getDateDifference(dateOne, dateTwo);
        System.out.println(message);

        /**тест на часы*/
        date1 = "02.04.18 00:25";
        date2 = "02.04.18 07:26";
        dateOne = format.parse(date1);
        dateTwo = format.parse(date2);
        message = DateUtils.getDateDifference(dateOne, dateTwo);
        System.out.println(message);

        /**тест на дни*/
        date1 = "02.04.18 00:25";
        date2 = "03.04.18 01:25";
        dateOne = format.parse(date1);
        dateTwo = format.parse(date2);
        message = DateUtils.getDateDifference(dateOne, dateTwo);
        System.out.println(message);

        /**тест на больше 2 дней*/
        date1 = "02.04.18 00:25";
        date2 = "04.04.18 01:25";
        dateOne = format.parse(date1);
        dateTwo = format.parse(date2);
        message = DateUtils.getDateDifference(dateOne, dateTwo);
        System.out.println(message);

        /**тест на меньше 1 минуты (только что)*/
        date1 = "05.04.18 00:25";
        date2 = "05.04.18 00:26";
        dateOne = format.parse(date1);
        dateTwo = format.parse(date2);
        message = DateUtils.getDateDifference(dateOne, dateTwo);
        System.out.println(message);
    }
}