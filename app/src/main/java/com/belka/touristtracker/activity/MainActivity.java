package com.belka.touristtracker.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.belka.touristtracker.Application;
import com.belka.touristtracker.PositionController;
import com.belka.touristtracker.R;
import com.belka.touristtracker.model.Contact;
import com.belka.touristtracker.model.GeoDatePoint;
import com.belka.touristtracker.service.ContactService;
import com.belka.touristtracker.service.GPSListener;
import com.belka.touristtracker.service.GPSService;
import com.belka.touristtracker.service.SMSCallBack;
import com.belka.touristtracker.service.SMSService;
import com.belka.touristtracker.utils.ContactHelper;

import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.map)
    MapView map;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    //@BindView(R.id.contactsOnMapMenu)
    //MenuItem contactsOnMapMenu;


    private final static int PICK_CONTACT_FOR_SEND_POSITION = 444;
    private final static int PICK_CONTACT_FOR_GET_POSITION = 445;

    private final static String[] PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_SMS
    };

    private final static int REQUEST_PERMISSION = 455;

    private GPSService gpsService;
    private SMSService smsService;
    private ContactService contactService;
    private PositionController positionController;


    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean permission = checkPermission(PERMISSIONS);

        if (!permission) {
            requestPermission(PERMISSIONS);

            Toast.makeText(this, "Insufficent rights. Application will be closed!", Toast.LENGTH_LONG).show();
            finish();
        }



        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        //setting this before the layout is inflated is a good idea
        //it 'should' ensure that the map has a writable location for the map cache, even without permissions
        //if no tiles are displayed, you can try overriding the cache path using Configuration.getInstance().setCachePath
        //see also StorageUtils
        //note, the load method also sets the HTTP User Agent to your application's package name, abusing osm's tile servers will get you banned based on this string

        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);

        Configuration.getInstance().setDebugMode(true);
        Configuration.getInstance().setDebugMapTileDownloader(true);

        gpsService = Application.getInstance().getGPSService();

        smsService = Application.getInstance().getSmsService();

        contactService = Application.getInstance().getContactSerivce();
        contactService.setMap(map);
        contactService.setActivity(this);
        contactService.setContactsOnMapMenu(navigationView.getMenu().findItem(R.id.contactsOnMapMenu));
        contactService.loadContacts();
        contactService.addMarkersOnMap();

        positionController = Application.getInstance().getPositionController();
        positionController.setMap(map);
        positionController.init();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public void requestPermission(String... permissions) {
        if (Build.VERSION.SDK_INT >= 23) {
            //for (String permission : permissions) {
              //  if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSION);
              //  }
        }
    }

    public boolean checkPermission(String... permissions) {
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public boolean isPermissionGranted(String... permissions) {
        if (Build.VERSION.SDK_INT >= 23) {
            for (String permission : permissions) {
              if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                  return false;
              }
            }
        }
        return true;
    }



    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (PICK_CONTACT_FOR_SEND_POSITION) :
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Contact contact = ContactHelper.getContactByURI(contactData);
                    if (contact != null) {
                        if (isPermissionGranted(Manifest.permission.SEND_SMS)) {
                            smsService.sendPosition(contact, new SMSCallBack() {
                                @Override
                                public void onSuccess() {
                                    Snackbar.make(fab, "SMS Message sent", Snackbar.LENGTH_LONG)
                                            .setAction("Action ", null).show();
                                }

                                @Override
                                public void onError() {

                                }
                            });

                        }
                    }
                }
                break;
            case PICK_CONTACT_FOR_GET_POSITION:
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Contact contact = ContactHelper.getContactByURI(contactData);
                    if (contact != null) {
                        if (isPermissionGranted(Manifest.permission.SEND_SMS)) {
                            smsService.sendRequestForPosition(contact.getNumber(), new SMSCallBack() {
                                @Override
                                public void onSuccess() {
                                    Snackbar.make(fab, "SMS for get position sent", Snackbar.LENGTH_LONG)
                                            .setAction("Action ", null).show();
                                }

                                @Override
                                public void onError() {

                                }
                            });

                        }
                    }

                }
                break;
        }
    }

    @OnClick(R.id.set_position)
    void onClickSetPosition(View view) {
        positionController.goToCenter();
    }

    @OnClick(R.id.get_position)
    void onClickGetPosition(View view) {
        if (isPermissionGranted(Manifest.permission.READ_CONTACTS)) {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, PICK_CONTACT_FOR_GET_POSITION);
        }
    }


    @OnClick(R.id.fab)
    public void onClickFab(View view) {
        if (isPermissionGranted(Manifest.permission.READ_CONTACTS)) {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, PICK_CONTACT_FOR_SEND_POSITION);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

       /* if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onResume(){
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
        positionController.onResume();
        ContactService Service = new ContactService();
        Service.refreshMarker();
    }

    public void onPause(){
        super.onPause();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        map.onPause();  //needed for compass, my location overlays, v6.0.0 and up
        positionController.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                for (int i : grantResults) {
                    if (i != PackageManager.PERMISSION_GRANTED) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage("App needs permisions!")
                            /*.setPositiveButton(R.string.fire, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // FIRE ZE MISSILES!
                                }
                            })*/
                                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // User cancelled the dialog
                                        System.exit(-1);
                                    }
                                });
                        // Create the AlertDialog object and return it
                        builder.create().show();
                        return;
                    }
                }
            }
        }
    }

}
