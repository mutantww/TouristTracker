package com.belka.touristtracker.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.belka.touristtracker.Application;
import com.belka.touristtracker.R;
import com.belka.touristtracker.model.Contact;
import com.belka.touristtracker.model.GeoContact;
import com.belka.touristtracker.model.GeoDatePoint;
import com.belka.touristtracker.service.ContactService;
import com.belka.touristtracker.utils.LocationUtil;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsActivity extends AppCompatActivity {

    ContactService contactService;

    GeoContact geoContact;

    @BindView(R.id.contactName)
    TextView contactName;

    @BindView(R.id.contactNumber)
    TextView contactNumber;

    @BindView(R.id.location)
    TextView contactLocation;

    @BindView(R.id.locationReceived)
    TextView contactLocationReceived;

    GeoDatePoint location;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ContactService.DATE_FORMAT);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        Contact contact = (Contact)this.getIntent().getExtras().get("contact");
        contactService = ((Application)this.getApplication()).getContactSerivce();

        geoContact = contactService.find(contact);

        setTitle(contact.getName() != null ? geoContact.getName() : "");

        contactName.setText(contact.getName() != null ? geoContact.getName() : "");

        contactNumber.setText(geoContact.getNumber());

        location = geoContact.getLocation();

        String locationString = LocationUtil.formatLocation(location);
        contactLocation.setText(locationString);

        contactLocationReceived.setText(simpleDateFormat.format(location.getDate()));
    }

    @OnClick(R.id.shareButton)
    void onClickShare() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, buildShareLink(location, geoContact));
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @OnClick(R.id.showOnMap)
    void onClickShowOnMap() {
        contactService.showOnCenter(geoContact);
        finish();
    }

    @OnClick(R.id.delete)
    void onClickDelete() {
        contactService.deleteContact(geoContact);
        finish();
    }

    /*private String buildShareLink(GeoDatePoint location, GeoContact geoContact) {
        String lat = LocationUtil.formatDouble(location.getLatitude());
        String lon = LocationUtil.formatDouble(location.getLongitude());
        String link = "https://www.openstreetmap.org/?mlat=%s&mlon=%s&zoom=15";

        StringBuilder builder = new StringBuilder();

        String name = geoContact.getName() != null ? geoContact.getName() + "(" + geoContact.getNumber() + ")"
                : geoContact.getNumber();

        builder.append("<p>");
        builder.append("<a href=\"" + String.format(link, lat, lon)+ "\">" + name + "</a>");
        builder.append("</p>");
        return builder.toString();
    }*/

    private String buildShareLink(GeoDatePoint location, GeoContact contact) {
        String lat = LocationUtil.formatDouble(location.getLatitude());
        String lon = LocationUtil.formatDouble(location.getLongitude());
        String link = "https://www.openstreetmap.org/?mlat=%s&mlon=%s&zoom=15";

        return String.format(link, lat, lon);
    }


}
