package com.belka.touristtracker;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;

import com.belka.touristtracker.model.ResponseTask;
import com.belka.touristtracker.service.GPSListener;
import com.belka.touristtracker.service.GPSService;

import org.osmdroid.api.IMapController;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

/**
 * Created by Volodymyr on 09.03.2018.
 */

public class PositionController {

    private Marker i;

    private MainActivityGPSListener gpsListener;
    private CompassListener compassListener;
    Sensor accelerometer;
    Sensor magnetometer;

    private GPSService gpsService;
    private MapView map;

    private SensorManager mSensorManager;


    public void setGpsService(GPSService gpsService) {
        this.gpsService = gpsService;
    }

    public void setMap(MapView map) {
        this.map = map;
    }

    class MainActivityGPSListener implements GPSListener {

        private boolean positionToCenter;

        MainActivityGPSListener(boolean positionToCenter) {
            this.positionToCenter = positionToCenter;
        }

        public void goToCenter() {
            this.positionToCenter = true;
        }

        @Override
        public void onLocationChanged(Location location) {
            IMapController mapController = map.getController();

            GeoPoint myPoint = gpsService.getCurrentPosition();

            if (i == null) {
                i = new Marker(map);
                i.setIcon(Application.getInstance().getResources().getDrawable(R.drawable.direction_arrow));
                map.getOverlays().add(i);
                i.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                i.setAnchor(0.5f, 0.5f);
            }
            i.setPosition(myPoint);

            if (positionToCenter) {
                if (map.getZoomLevel() < 14)
                    mapController.setZoom(14);
                mapController.setCenter(myPoint);
                positionToCenter = false;
            }

        }
    }

    private class CompassListener implements SensorEventListener {
        private static final float alpha = 0.6f;

        private float[] mGravity = new float[3];
        private float[] mGeomagnetic = new float[3];


        @Override
        public void onSensorChanged(SensorEvent event) {

            synchronized (this) {
                if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                    mGravity[0] = alpha * mGravity[0] + (1 - alpha)
                            * event.values[0];
                    mGravity[1] = alpha * mGravity[1] + (1 - alpha)
                            * event.values[1];
                    mGravity[2] = alpha * mGravity[2] + (1 - alpha)
                            * event.values[2];
                }

                if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                    mGeomagnetic[0] = alpha * mGeomagnetic[0] + (1 - alpha)
                            * event.values[0];
                    mGeomagnetic[1] = alpha * mGeomagnetic[1] + (1 - alpha)
                            * event.values[1];
                    mGeomagnetic[2] = alpha * mGeomagnetic[2] + (1 - alpha)
                            * event.values[2];
                }
            }
            if (mGravity != null && mGeomagnetic != null) {
                float R[] = new float[9];
                float I[] = new float[9];

                if (SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic)) {

                    // orientation contains azimut, pitch and roll
                    float orientation[] = new float[3];
                    SensorManager.getOrientation(R, orientation);

                    float azimut = (float) Math.toDegrees(orientation[0]);
                    azimut = (azimut + 360) % 360;

                    //float rotation = -azimut * 360 / (2 * 3.14159f);

                    if (i != null) {
                        i.setRotation(azimut);
                        map.invalidate();
                    }
                }

            }


        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    }

    public void init() {
        if (ActivityCompat.checkSelfPermission(Application.getInstance(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(Application.getInstance(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (gpsListener == null) {
                gpsListener = new MainActivityGPSListener(true);
            }

            gpsService.addListener(gpsListener);
        }

        if (compassListener == null) {
            compassListener = new CompassListener();
        }

        mSensorManager = (SensorManager) Application.getInstance().getSystemService(Application.getInstance().SENSOR_SERVICE);


        accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

    }

    public void goToCenter() {
        if (gpsListener != null)
            gpsListener.goToCenter();
    }

    public void onResume() {
        gpsService.addListener(gpsListener);

        mSensorManager.registerListener(compassListener, accelerometer, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(compassListener, magnetometer, SensorManager.SENSOR_DELAY_UI);
    }

    public void onPause() {
        gpsService.removeListener(gpsListener);
        mSensorManager.unregisterListener(compassListener);
    }
}
