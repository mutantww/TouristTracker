package com.belka.touristtracker.service;

/**
 * Created by Volodymyr on 25.07.2018.
 */

public interface SMSCallBack {
    void onSuccess();
    void onError();
}
