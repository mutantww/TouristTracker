package com.belka.touristtracker.service;
import android.location.Location;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;

import com.belka.touristtracker.Application;
import com.belka.touristtracker.model.Contact;
import com.belka.touristtracker.model.GeoContact;
import com.belka.touristtracker.model.ResponseTask;
import com.belka.touristtracker.utils.ContactHelper;
import com.belka.touristtracker.model.GeoDatePoint;
import com.belka.touristtracker.utils.LocationUtil;

import org.osmdroid.util.GeoPoint;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by Volodymyr on 01.03.2018.
 */

public class SMSService {

    static final String GET_MARKER = "ttget";
    static final String PUT_MARKER = "ttput";

    private GPSService gpsService;
    private NotificationService notificationService;

    private List<SMSPositionListener> positionListeners;

    public void init() {
        positionListeners = new ArrayList<>();
    }

    public void analyze(SmsMessage smsMessage) throws ParseException {
        String messageBody = smsMessage.getMessageBody();
        final String number = smsMessage.getDisplayOriginatingAddress();
        final Contact contact = ContactHelper.getContactByNumber(number);
        if (messageBody.startsWith(GET_MARKER)) {

            gpsService.addTask(contact, new GPSTimeOutListener() {
                @Override
                public void onLocationChanged(Location location) {
                    sendPosition(contact.getNumber(), new GeoDatePoint(location));
                }

                @Override
                public void onTimeOut(ResponseTask task) {
                    notificationService.notify(2, task.getContact(),
                            "Error", " Error send position to " + task.getContact().getName());
                }
            });


        } else if (messageBody.startsWith(PUT_MARKER)) {
            GeoDatePoint point = parseString(messageBody);
            for (SMSPositionListener listener : positionListeners) {
                listener.onPutPosition(contact, point, smsMessage);
            }
        }
    }

    public void sendPosition(Contact contact, SMSCallBack callBack) {
        try {
            gpsService.addTask(contact, new GPSTimeOutListener() {
                        @Override
                        public void onLocationChanged(Location location) {
                            sendPosition(contact.getNumber(), new GeoDatePoint(location));
                            callBack.onSuccess();
                        }

                        @Override
                        public void onTimeOut(ResponseTask task) {
                            notificationService.notify(2, task.getContact(), "Error", " Error send position to " + task.getContact().getName());
                            callBack.onError();
                        }
                    }
            );
        } catch (Exception ex) {
            callBack.onError();
        }

    }

    private void sendPosition(String phoneNumber, GeoDatePoint point) {
        sendSMS(phoneNumber, buildSmsGpsMesg(PUT_MARKER, point, point.getDate()));
    }

    public void sendRequestForPosition(String phoneNumber, SMSCallBack callBack) {
        try {
            sendSMS(phoneNumber, buildSmsGpsMesg(GET_MARKER, null, null));
            callBack.onSuccess();
        } catch (Exception ex) {
            callBack.onError();
        }
    }

    public void sendSMS(String phoneNumber, String body) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(phoneNumber,
                null, body,
                null, null);
    }

    public void setGPSService(GPSService gpsService) {
        this.gpsService = gpsService;
    }

    private String buildSmsGpsMesg(String marker, GeoPoint point, Date date) {
        StringBuilder builder = new StringBuilder(marker);
        if (point != null) {
            builder.append(" ").append(LocationUtil.formatDouble(point.getLatitude())).append(" ")
                    .append(LocationUtil.formatDouble(point.getLongitude()));
        }
        if (date != null) {
            builder.append(" ").append(LocationUtil.dateToSmsTimeStamp(date));
        }
        return builder.toString();
    }

    private GeoDatePoint parseString(String str) throws ParseException {
        String [] paths = str.split(" ");
        Double lat = LocationUtil.parseDouble(paths[1]);
        Double lon = LocationUtil.parseDouble(paths[2]);

        Date date = LocationUtil.parseSmsTimeStamp(paths[3]);

        GeoDatePoint point = new GeoDatePoint(lat, lon, date);
        return point;
    }

    public void addListener(SMSPositionListener smsPositionListener) {
        positionListeners.add(smsPositionListener);
    }

    public void removeListener(SMSPositionListener smsPositionListener) {
        positionListeners.remove(smsPositionListener);
    }


    public NotificationService getNotificationService() {
        return notificationService;
    }

    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
    }
}
