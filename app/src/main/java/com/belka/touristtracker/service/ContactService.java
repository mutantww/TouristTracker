package com.belka.touristtracker.service;

import android.app.Activity;
import android.content.Intent;
import android.preference.Preference;
import android.view.MenuItem;

import com.belka.touristtracker.Application;
import com.belka.touristtracker.PositionController;
import com.belka.touristtracker.activity.DetailsActivity;
import com.belka.touristtracker.model.Contact;
import com.belka.touristtracker.model.GeoContact;
import com.belka.touristtracker.utils.PreferencesUtils;
import com.google.gson.reflect.TypeToken;

import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import static com.belka.touristtracker.utils.DateUtils.getDateDifference;

/**
 * Created by Volodymyr on 01.03.2018.
 */

public class ContactService {

    private static Type CONTACT_LIST_TYPE = new TypeToken<ArrayList<GeoContact>>(){}.getType();

    public static String DATE_FORMAT = "dd.MM.yy HH:mm";

    private MapView map;
    private SMSService smsService;
    private List<GeoContact> contacts;
    //private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
    private MenuItem contactsOnMapMenu;
    private Activity activity;
    private PositionController positionController;
    private boolean init;


    public void init() {

        smsService.addListener((androidContact, point, smsMessage) -> {
            GeoContact contact = find(androidContact);
            if (contact != null) {
                contact.setLocation(point);
                contact.getMarker().setPosition(contact.getLocation());
                contact.getMarker().showInfoWindow();
            } else {
                contact = new GeoContact(androidContact, point);

                Marker marker = createMarker(contact);
                map.getOverlays().add(marker);

                contacts.add(contact);

                addContactToMenu(contact);

                saveContacts();
            }
        });
        init = true;
    }

    public void showOnCenter(GeoContact contact) {
        if (contact != null) {
            if (map.getZoomLevel() < 14)
                map.getController().setZoom(14);
            map.getController().setCenter(contact.getLocation());
            contact.getMarker().showInfoWindow();
            map.invalidate();
        }
    }

    public void setSmsService(SMSService smsService) {
        this.smsService = smsService;
    }

    public GeoContact find(Contact androidContact) {
        for (GeoContact contact : contacts) {
            if (contact.getContact().equals(androidContact)) {
                return contact;
            }
        }
        return null;
    }

    public void setMap(MapView map) {
        this.map = map;
    }

    public void addMarkersOnMap() {
        for (GeoContact contact: contacts) {
            map.getOverlays().remove(contact.getMarker());

            Marker marker = createMarker(contact);

            map.getOverlays().add(marker);
        }
    }


    public void refreshMarker() {
        if(init == true) {
            if (contacts.size() > 0) {
                for (GeoContact contact : contacts) {
                    Marker marker = contact.getMarker();
                    marker.setTitle(buildTtitle(contact));
                }
            }
        }
    }


    private String buildTtitle(GeoContact contact) {
        StringBuilder builder = new StringBuilder();
        Date currentDate = new Date();
        if (contact.getName() != null) {
            builder.append(contact.getName() + "(" + contact.getNumber() + ")");
        } else {
            builder.append(contact.getNumber());
        }
        builder.append("\n");
        builder.append(getDateDifference(contact.getLocation().getDate(),currentDate));
        return builder.toString();
    }

    public void setContactsOnMapMenu(MenuItem contactsOnMapMenu) {
        this.contactsOnMapMenu = contactsOnMapMenu;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void deleteContact(GeoContact contact) {
        if (contact != null) {
            contacts.remove(contact);
            contactsOnMapMenu.getSubMenu().removeItem(contact.getMenuItem().getItemId());
            map.getOverlays().remove(contact.getMarker());
            map.invalidate();
            positionController.goToCenter();
            saveContacts();
        }
    }

    public void setPositionController(PositionController positionController) {
        this.positionController = positionController;
    }

    public void saveContacts() {
        PreferencesUtils.writeValue(Application.getInstance(), "contactList", contacts);
    }

    public void loadContacts() {
        contacts = new ArrayList<>(PreferencesUtils.getValue(Application.getInstance(), "contactList",
                new ArrayList<GeoContact>(), CONTACT_LIST_TYPE));

        for (GeoContact geoContact : contacts) {
            Marker marker = createMarker(geoContact);
            map.getOverlays().add(marker);

            addContactToMenu(geoContact);
        }
    }

    private Marker createMarker(GeoContact contact) {
        Marker marker = new Marker(map);
        marker.setTitle(buildTtitle(contact));
        marker.setPosition(contact.getLocation());
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        marker.showInfoWindow();
        marker.setOnMarkerClickListener((inMarker, mapView) -> {
            Intent intent = new Intent(map.getContext(), DetailsActivity.class);
            intent.putExtra("contact", contact.getContact());
            activity.startActivity(intent);
            return false;
        });

        contact.setMarker(marker);
        return marker;
    }

    private void addContactToMenu(GeoContact contact) {
        MenuItem menuItem = contactsOnMapMenu.getSubMenu().add(
                contact.getName() != null ? contact.getName() : contact.getNumber());
        menuItem.setOnMenuItemClickListener(item -> {
            Intent intent = new Intent(map.getContext(), DetailsActivity.class);
            intent.putExtra("contact", contact.getContact());
            activity.startActivity(intent);
            return false;

        });
        contact.setMenuItem(menuItem);
    }
}
