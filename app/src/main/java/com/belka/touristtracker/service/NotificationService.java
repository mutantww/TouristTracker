package com.belka.touristtracker.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

import com.belka.touristtracker.Application;
import com.belka.touristtracker.R;
import com.belka.touristtracker.activity.DetailsActivity;
import com.belka.touristtracker.activity.MainActivity;
import com.belka.touristtracker.model.Contact;

/**
 * Created by Volodymyr on 01.04.2018.
 */

public class NotificationService {

    private SMSService smsService;

    private static int NOTIFICATION_ID = 1;

    public void init() {
        smsService.addListener((contact, point, smsMessage) -> {
            notify(NOTIFICATION_ID, contact, "New location",
                    "new location from " + contact.getName() != null ? contact.getName() : contact.getNumber());
        });
    }

    public void setSmsService(SMSService smsService) {
        this.smsService = smsService;
    }

    void notify(int id, Contact contact, String title, String text) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(Application.getInstance())
                        .setSmallIcon(R.drawable.direction_arrow)
                        .setContentTitle(title)
                        .setContentText(text);

        /*Intent resultIntent = new Intent(Application.getInstance(), DetailsActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        resultIntent.putExtra("contact", contact);
        */
        /*PendingIntent resultPending
        Intent = PendingIntent.getActivity(Application.getInstance(),
                0, resultIntent, PendingIntent.FLAG_CANCEL_CURRENT);
*/
        /*
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(Application.getInstance());
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        */
        // Create an Intent for the activity you want to start
        Intent resultIntent = new Intent(Application.getInstance(), DetailsActivity.class);
        resultIntent.putExtra("contact", contact);
// Create the TaskStackBuilder and add the intent, which inflates the back stack
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(Application.getInstance());
        stackBuilder.addNextIntentWithParentStack(resultIntent);
// Get the PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setAutoCancel(true);
        mBuilder.setPriority(android.app.Notification.PRIORITY_MAX);
        mBuilder.setLights(Color.RED, 500, 500);

        mBuilder.setVibrate(new long[]{ 1000, 1000});
        mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

        NotificationManager mNotificationManager = (NotificationManager) Application.getInstance().
                getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(id, mBuilder.build());
    }

}
