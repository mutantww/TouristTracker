package com.belka.touristtracker.service;

import android.location.Location;
import android.telephony.SmsMessage;

import com.belka.touristtracker.model.Contact;
import com.belka.touristtracker.model.GeoDatePoint;

import org.osmdroid.util.GeoPoint;

/**
 * Created by Volodymyr on 01.03.2018.
 */

public interface SMSPositionListener {
    void onPutPosition(Contact contact, GeoDatePoint location, SmsMessage smsMessage);
}
