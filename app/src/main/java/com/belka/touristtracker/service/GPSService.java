package com.belka.touristtracker.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.belka.touristtracker.Application;
import com.belka.touristtracker.model.Contact;
import com.belka.touristtracker.model.GeoDatePoint;
import com.belka.touristtracker.model.ResponseTask;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by Volodymyr on 01.03.2018.
 */

public class GPSService {

    final long TIMEOUT = 60*1000L;

    private Location currentLocation;
    private LocationManager locationManager;
    private Set<ResponseTask> responseTasks;

    Map<GPSListener, LocationListener> locationListenerMap = new HashMap<>();


    private static final float MIN_DISTANCE = 2;
    private static final long VALID_TIME_OUT = 5;

    public void init() {
        locationManager = (LocationManager) Application.getInstance().getSystemService(Context.LOCATION_SERVICE);
        responseTasks = new LinkedHashSet<>();
    }


    public GeoDatePoint getCurrentPosition() {
        return new GeoDatePoint(currentLocation);
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public boolean isActualPosition() {
        if (currentLocation == null)
            return false;
        if (new Date().getTime() - currentLocation.getTime() <= VALID_TIME_OUT * 1000)
            return true;
        return false;
    }

    @SuppressLint("MissingPermission")
    public void addListener(final GPSListener gpsListener) {
        LocationListener locationListener = locationListenerMap.get(gpsListener);

        if (locationListener != null) {
            return;
        }

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                currentLocation = location;
                gpsListener.onLocationChanged(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        };

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, MIN_DISTANCE, locationListener);
        locationListenerMap.put(gpsListener, locationListener);
    }


    @SuppressLint("MissingPermission")
    public void addTask(Contact contact, GPSTimeOutListener gpsListener) {
        if (isActualPosition()) {
            gpsListener.onLocationChanged(getCurrentLocation());
        } else {
            ResponseTask task = new ResponseTask();
            responseTasks.add(task);
            task.setContact(contact);

            Timer timer = new Timer();

            final LocationListener locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    gpsListener.onLocationChanged(location);
                    responseTasks.remove(task);
                    locationManager.removeUpdates(this);
                    timer.cancel();
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            };

            task.setTimer(timer);

            task.setLocationListener(locationListener);

            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    locationManager.removeUpdates(task.getLocationListener());
                    task.getTimer().cancel();
                    gpsListener.onTimeOut(task);
                    responseTasks.remove(task);
                }
            }, TIMEOUT);


            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, MIN_DISTANCE, locationListener);


        }

    }

    public void removeListener(GPSListener gpsListener) {
        LocationListener locationListener = locationListenerMap.get(gpsListener);
        if (locationListener != null) {
            locationManager.removeUpdates(locationListener);
            locationListenerMap.remove(gpsListener);
        }
    }


}