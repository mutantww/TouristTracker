package com.belka.touristtracker.service;

import com.belka.touristtracker.model.ResponseTask;

/**
 * Created by Volodymyr on 25.07.2018.
 */

public interface GPSTimeOutListener extends GPSListener {
    void onTimeOut(ResponseTask task);
}
