package com.belka.touristtracker.service;

import android.location.Location;

/**
 * Created by Volodymyr on 01.03.2018.
 */

public interface GPSListener {
    void onLocationChanged(Location location);
}
