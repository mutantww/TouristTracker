package com.belka.touristtracker;

import com.belka.touristtracker.service.ContactService;
import com.belka.touristtracker.service.GPSService;
import com.belka.touristtracker.service.NotificationService;
import com.belka.touristtracker.service.SMSService;

/**
 * Created by Volodymyr on 01.03.2018.
 */

public class Application extends android.app.Application {
    private GPSService gpsService;
    private SMSService smsService;
    private ContactService contactService;
    private static Application instance;
    private PositionController positionController;
    private NotificationService notificationService;


    public static Application getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        gpsService = new GPSService();
        gpsService.init();

        notificationService = new NotificationService();
        smsService = new SMSService();

        notificationService.setSmsService(smsService);
        smsService.setGPSService(gpsService);
        smsService.setNotificationService(notificationService);

        smsService.init();

        notificationService.init();

        positionController = new PositionController();
        positionController.setGpsService(gpsService);

        contactService = new ContactService();
        contactService.setSmsService(smsService);
        contactService.setPositionController(positionController);
        contactService.init();


    }

    public GPSService getGPSService() {
        return gpsService;
    }

    public SMSService getSmsService() {
        return smsService;
    }

    public ContactService getContactSerivce() {
        return contactService;
    }

    public PositionController getPositionController() {
        return positionController;
    }

    public void setPositionController(PositionController positionController) {
        this.positionController = positionController;
    }

    public NotificationService getNotificationService() {
        return notificationService;
    }
}
