package com.belka.touristtracker.utils;

import com.belka.touristtracker.model.GeoDatePoint;

import org.osmdroid.util.GeoPoint;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by Volodymyr on 27.03.2018.
 */

public class LocationUtil {
    private static DecimalFormat decimalFormat;
    static {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormat = new DecimalFormat("###.#####", decimalFormatSymbols);
    }

    static final Long INIT_TIME_IN_SECONDS = 1262304L;
    static final int RADIX = 36;

    public static String formatDouble(double d) {
        return decimalFormat.format(d);
    }

    public static String formatLocation(GeoPoint point) {
        return decimalFormat.format(point.getLatitude()) + ", " + decimalFormat.format(point.getLongitude());
    }

    public static Double parseDouble(String str) throws ParseException {
        return decimalFormat.parse(str).doubleValue();
    }

    public static Date parseSmsTimeStamp(String str) {
        Long dateOffset = Long.parseLong(str, RADIX);

        return new Date((dateOffset + INIT_TIME_IN_SECONDS) * 1000);
    }

    public static String dateToSmsTimeStamp(Date date) {
        return Long.toString(date.getTime() / 1000 - INIT_TIME_IN_SECONDS, RADIX);
    }

}
