package com.belka.touristtracker.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.lang.reflect.Type;

/**
 * Created by Volodymyr on 10.12.2017.
 */

public class PreferencesUtils {
    public static void writeValue(Context context, String key, Object value) {
        SharedPreferences sharedPref = context.getSharedPreferences("com.belka.tt",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new Gson();
        editor.putString(key, gson.toJson(value));
        editor.commit();
    }

    public static <T> T getValue(Context context, String key, T defValue, Type type) {
        SharedPreferences sharedPref = context.getSharedPreferences("com.belka.tt", Context.MODE_PRIVATE);
        String value = sharedPref.getString(key, "");
        if (value.equals("")) {
            return defValue;
        }
        Gson gson = new Gson();
        return gson.fromJson(value, type);
    }

}
