package com.belka.touristtracker.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Volodymyr on 28.03.2018.
 */

public class DateUtils {
    public static String DATE_FORMAT = "dd.MM.yy HH:mm";

    public static String getDateDifference(Date date1, Date date2) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        String message = "";     //строка для сообщения о последнем заходе

        /**устанавливаем критерии разниц дат*/
        long differenceOne = TimeUnit.MINUTES.toMillis(1);      //одна минута
        long differenceTwo = TimeUnit.HOURS.toMillis(5);        //больше пяти часов
        long differenceThree = TimeUnit.DAYS.toMillis(2);       //больше двух дней

        /**вычисляем разницу дат*/
        long currentDifference = date2.getTime() - date1.getTime();
        long days = TimeUnit.MILLISECONDS.toDays(currentDifference);
        long hours = TimeUnit.MILLISECONDS.toHours(currentDifference);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(currentDifference);

        if (currentDifference <= differenceOne) {
            message += " ";
            message += format.format(date1);
        }

        if ((currentDifference > differenceOne) && (currentDifference <= differenceTwo)) {
            if (hours == 1) {
                message += " 1 hour";
            } else if (hours > 1) {
                message += " ";
                message += hours;
                message += " hours";
            } else if (minutes % 60 == 1) {
                message += " 1 minute";
            } else if ((minutes % 60 > 1) && (minutes % 60 != 1)) {
                message += " ";
                message += minutes % 60;
                message += " minutes";
            }
            message += " ago.";
        }

        if ((currentDifference > differenceTwo) && (currentDifference < differenceThree)) {
            if (days == 1) {
                message += " 1 day";
            } else if (days > 1) {
                message += " ";
                message += days;
                message += " days";
            } else if (hours % 24 == 1) {
                message += " 1 hour";
            } else if ((hours % 24 > 1) && (hours % 24 != 1)) {
                message += " ";
                message += hours % 24;
                message += " hours";
            }
            message += " ago.";
        }

        if (currentDifference >= differenceThree) {
            message += " ";
            message += format.format(date1);
        }

        return message;
    }
}
