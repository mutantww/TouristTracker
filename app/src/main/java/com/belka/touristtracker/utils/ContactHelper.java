package com.belka.touristtracker.utils;

import android.Manifest;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;

import com.belka.touristtracker.Application;
import com.belka.touristtracker.model.Contact;
import com.belka.touristtracker.service.SMSCallBack;

import java.net.URI;

/**
 * Created by Volodymyr on 08.03.2018.
 */

public class ContactHelper {

    public static Contact getContactByNumber(final String phoneNumber) {
        Contact contact = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        String[] projection = new String[] {
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup.NUMBER,
                ContactsContract.PhoneLookup.CONTACT_ID
        };


        Cursor cursor = Application.getInstance().getContentResolver().query(uri,projection,null,null,null);

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                String contactName = cursor.getString(0);
                String number = cursor.getString(1);
                String id = cursor.getString(2);
                contact = new Contact(id, number, contactName);
            }
            cursor.close();
        }

        return contact;
    }

    public static Contact getContactByURI(Uri contactURI) {
        Cursor cur = Application.getInstance().getContentResolver().query(contactURI, null, null, null, null);
        if (cur.getCount() > 0) {
            if(cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor phones = Application.getInstance().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,null, null);
                    if (phones.moveToNext()) {
                        String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        return new Contact(id, name, phoneNumber);
                    }
                    phones.close();
                }

            }
        }
        cur.close();
        return null;
    }
}
