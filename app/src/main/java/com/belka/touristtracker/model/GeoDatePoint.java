package com.belka.touristtracker.model;

import android.location.Location;

import org.osmdroid.util.GeoPoint;

import java.util.Date;

/**
 * Created by Volodymyr on 08.03.2018.
 */

public class GeoDatePoint extends GeoPoint {
    private Date date;
    public GeoDatePoint(GeoPoint point) {
        super(point);
        this.date = new Date();
    }

    public GeoDatePoint(Double lat, Double lon, Date date) {
        super(lat, lon);
        this.date = date;
    }

    public GeoDatePoint(Location location) {
        super(location);
        this.date = new Date(location.getTime());
    }

    public Date getDate() {
        return date;
    }

}
