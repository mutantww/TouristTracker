package com.belka.touristtracker.model;

import android.location.LocationListener;

import java.util.Timer;

/**
 * Created by Volodymyr on 02.04.2018.
 */

public class ResponseTask {
    private Contact contact;
    private Timer timer;
    private LocationListener locationListener;

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public LocationListener getLocationListener() {
        return locationListener;
    }

    public void setLocationListener(LocationListener locationListener) {
        this.locationListener = locationListener;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
}
