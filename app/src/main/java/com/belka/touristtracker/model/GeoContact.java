package com.belka.touristtracker.model;

import android.location.Location;
import android.view.MenuItem;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.Marker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Volodymyr on 01.03.2018.
 */

public class GeoContact implements Serializable {

    private Contact contact;
    List<GeoDatePoint> locationList = new ArrayList<>();
    private transient Marker marker;
    private transient MenuItem menuItem;

    public GeoContact(Contact contact, GeoDatePoint location) {
        this.setContact(contact);
        locationList.add(location);
    }

    public GeoDatePoint getLocation() {
        return locationList.get(locationList.size()-1);
    }

    public void setLocation(Location location) {
        locationList.add(new GeoDatePoint(location));
    }

    public void setLocation(GeoPoint location) {
        locationList.add(new GeoDatePoint(location));
    }

    public String getName() {
        return getContact().getName();
    }

    public String getNumber() {
        return getContact().getNumber();
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeoContact that = (GeoContact) o;

        return contact != null ? contact.equals(that.contact) : that.contact == null;
    }

    @Override
    public int hashCode() {
        return contact != null ? contact.hashCode() : 0;
    }
}
