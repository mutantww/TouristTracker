package com.belka.touristtracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import com.belka.touristtracker.service.GPSService;
import com.belka.touristtracker.service.SMSService;

import java.text.ParseException;

/**
 * Created by Volodymyr on 01.03.2018.
 */

public class SMSReciver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        SMSService smsService = ((Application)context.getApplicationContext()).getSmsService();

        if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                for (SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                    try {
                        smsService.analyze(smsMessage);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                // to do
            }
        }
    }
}